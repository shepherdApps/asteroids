//
//  Enemy.swift
//  Asteroids
//
//  Created by Michael Shepherd on 21/02/2021.
//

import Foundation
import SpriteKit

class Enemy: Object {
    
    private var velocity: CGPoint
    private var destinationRect: CGRect
    private var currentQuadrent: Int
    private let screenSize: CGSize
    private var bullets: [Bullet] = []
    
    // Variables needed for handling when the enemy fires a bullet
    private var generateBulletTimer: Int
    private var fireBulletTimer: Int
    private var fireBulletInterval: Int
    private let healthBar: HealthBar
    
    /*
        Initializes and sets up the Enemy
     */
    init(position: CGPoint, size: CGSize = Constants.ENEMY_SIZE, screenSize: CGSize) {
        self.velocity = CGPoint(x: 0, y: 0)
        self.screenSize = screenSize
        self.currentQuadrent = -1
        self.destinationRect = CGRect(x: 0, y: 0, width: Constants.BASE_ENEMY_SPEED + 0.1, height: Constants.BASE_ENEMY_SPEED + 0.1)
        self.generateBulletTimer = 0
        self.fireBulletTimer = 0
        self.fireBulletInterval = 0
        self.healthBar = HealthBar(position: CGPoint(x: 0, y: size.height / 2 + 20), size: CGSize(width: size.width, height: 10), startingHealth: Constants.BASE_ENEMY_HEALTH)
        super.init(position: position, texture: SKTexture(imageNamed: "Enemy"), size: size)
        
        self.name = "Enemy"
        self.physicsBody!.categoryBitMask = CollisionMask.Enemy.rawValue
        self.physicsBody!.contactTestBitMask = CollisionMask.Everything.rawValue
        self.physicsBody!.collisionBitMask = CollisionMask.Nothing.rawValue
        
        for _ in 1...Constants.BASE_ENEMY_NUM_BULLETS {
            let newBullet = Bullet(position: self.position, color: .red, fromEnemy: true)
            newBullet.zPosition = -2
            bullets.append(newBullet)
        }
        
        self.zPosition = -1
        self.setupRandomEnemyMovement()
        
        self.addChild(healthBar)
    }
    
    /*
        Required
     */
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Adds all the children of the enemy to the main scene object
     */
    public func addChildrenToScene(scene: SKScene) {
        for bullet in bullets {
            scene.addChild(bullet)
        }
    }
    
    /*
        Called once every frame - updates the enemy (specifically movement positions)
     */
    public override func update(screenSize: CGSize) {
        position.x += velocity.x
        position.y += velocity.y
        
        handleBulletFiring()
        
        if destinationRect.contains(position) {
            setupRandomEnemyMovement()
        }
        
        for bullet in bullets {
            bullet.update(screenSize: screenSize)
            
            if bullet.position != self.position && !bullet.getFired() {
                bullet.position = self.position
            }
        }
    }
    
    /*
        Handles updating all the bullet timers to work out when the enemy should fire a bullet
     */
    private func handleBulletFiring() {
        generateBulletTimer += 1
        fireBulletTimer += 1
        
        if fireBulletTimer >= fireBulletInterval && fireBulletInterval != 0 {
            fireBulletTimer = 0
            for bullet in bullets {
                if !bullet.getFired() {
                    bullet.setFired(fired: true)
                    break
                }
            }
        }
        
        if generateBulletTimer >= Constants.ENEMY_FIRE_INTERVAL {
            let randomBulletsInInterval = Int.random(in: 1...bullets.count)
            self.fireBulletInterval = Constants.ENEMY_FIRE_INTERVAL / randomBulletsInInterval
            generateBulletTimer = 0
        }
    }
    
    /*
        Handle all the computation required to generate the required velocities for the random enemy movement
     */
    public func setupRandomEnemyMovement() {
        let quadrent = getQuadrentBounds()
        let randomPosition = generateRandomPositionInQuadrent(quadrent: quadrent)
        destinationRect.origin = CGPoint(x: randomPosition.x, y: randomPosition.y)
        computeVelocityComponents()
    }
    
    /*
        Generate a random quadrent for the enemy to move to that is not equal to the current quadrent
    */
    private func getQuadrentBounds() -> CGRect {
        var newQuadrentNumber = currentQuadrent
        while newQuadrentNumber == currentQuadrent {
            newQuadrentNumber = Int.random(in: 0...3)
        }
        currentQuadrent = newQuadrentNumber
        
        switch newQuadrentNumber {
        case 0:
            return CGRect(x: size.width / 2, y: screenSize.height - screenSize.height / 4 + size.height / 2, width: screenSize.width / 2 - size.width, height: screenSize.height / 4 - size.height)
        case 1:
            return CGRect(x: screenSize.width / 2 + size.width / 2, y: screenSize.height - screenSize.height / 4 + size.height / 2, width: screenSize.width / 2 - size.width, height: screenSize.height / 4 - size.height)
        case 2:
            return CGRect(x: size.width / 2, y: screenSize.height / 2 + size.height / 2, width: screenSize.width / 2 - size.width, height: screenSize.height / 4 - size.height)
        case 3:
            return CGRect(x: screenSize.width / 2 + size.width / 2, y: screenSize.height / 2 + size.height / 2, width: screenSize.width / 2 - size.width, height: screenSize.height / 4 - size.height)
        default:
            print("Something went wrong with Enemy Movement")
            return CGRect()
        }
    }
    
    /*
        Generate a random position for the Enemy to move to within the provided quadrent
     */
    private func generateRandomPositionInQuadrent(quadrent: CGRect) -> CGPoint {
        let randomX = CGFloat.random(in: quadrent.minX...(quadrent.minX + quadrent.width))
        let randomY = CGFloat.random(in: quadrent.minY...(quadrent.minY + quadrent.height))
        return CGPoint(x: randomX, y: randomY)
    }
    
    /*
        Compute the values of the velocities x and y based on the position that the Enemy needs to end up at
     */
    private func computeVelocityComponents() {
        let xDistance = abs(self.destinationRect.minX + self.destinationRect.width / 2 - position.x)
        let yDistance = abs(self.destinationRect.minY + self.destinationRect.height / 2 - position.y)
        let angle = atan(yDistance / xDistance)
        
        if destinationRect.origin.x < position.x {
            velocity.x = -Constants.BASE_ENEMY_SPEED * cos(angle)
        }else {
            velocity.x = Constants.BASE_ENEMY_SPEED * cos(angle)
        }
        
        if destinationRect.origin.y < position.y {
            velocity.y = -Constants.BASE_ENEMY_SPEED * sin(angle)
        } else {
            velocity.y = Constants.BASE_ENEMY_SPEED * sin(angle)
        }
    }
    
    /*
        Decrease the health of the enemy by 1
     */
    public func decreaseHealth(){
        self.healthBar.decreaseHealth()
        
        if healthBar.getDead() {
            if let explosion = SKEmitterNode(fileNamed: "Explosion") {
                self.addChild(explosion)
            }
            let waitBlock = SKAction.wait(forDuration: 0.7)
            let runBlock = SKAction.run {
                self.removeFromParent()
            }
            let soundEffect = SKAction.playSoundFileNamed("explosion.wav", waitForCompletion: false)
            self.run(SKAction.sequence([soundEffect, waitBlock, runBlock]))
            
            for bullet in bullets {
                bullet.removeFromParent()
            }
        } else {
            self.takeDamage()
        }
    }
    
    public func getDead() -> Bool {
        return healthBar.getDead()
    }
}
