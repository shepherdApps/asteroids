//
//  MenuScene.swift
//  Asteroids
//
//  Created by Michael Shepherd on 23/02/2021.
//
//  Displays a menu at the start of the application that can be used for navigating to all other scenes
//

import Foundation
import SpriteKit
import GameKit

class MenuScene: SKScene {
    private var background: SKSpriteNode = SKSpriteNode()
    
    private var titleLabel: SKLabelNode = SKLabelNode()
    private var playLabel: SKLabelNode = SKLabelNode()
    private var infoLabel: SKLabelNode = SKLabelNode()
    private var highScoresLabel: SKLabelNode = SKLabelNode()
    private let screenSize: CGSize
    
    /*
        Sets up all elements of the MenuScene
     */
    init(screenSize: CGSize) {
        self.screenSize = screenSize
        super.init(size: screenSize)
        
        let backgroundTexture = SKTexture(imageNamed: "Background")
        let background = SKSpriteNode(texture: backgroundTexture)
        background.size = CGSize(width: size.width, height: size.height * 2)
        background.position = CGPoint(x: size.width / 2, y: size.height)
        self.addChild(background)
        
        titleLabel = setupLabelNode(text: "Asteroids", position: CGPoint(x: screenSize.width / 2, y: screenSize.height - 125))
        playLabel = setupLabelNode(text: "Play", position: CGPoint(x: screenSize.width / 2, y: screenSize.height - 250))
        infoLabel = setupLabelNode(text: "information", position: CGPoint(x: screenSize.width / 2, y: screenSize.height - 350))
        highScoresLabel = setupLabelNode(text: "High Scores", position: CGPoint(x: screenSize.width / 2, y: screenSize.height - 450))
        titleLabel.fontColor = .green
        
        self.addChild(titleLabel)
        self.addChild(playLabel)
        self.addChild(infoLabel)
        self.addChild(highScoresLabel)
    }
    
    /*
        Required Method
     */
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        let location = touch.location(in: self)
        
        if playLabel.frame.contains(location) {
            let revealTransition = SKTransition.reveal(with: .left, duration: 1)
            self.view?.presentScene(RocketScene(size: screenSize), transition: revealTransition)
        }
        else if infoLabel.frame.contains(location) {
            print("Pressed Info")
        }
        else if highScoresLabel.frame.contains(location) {
            print("Pressed High Scores")
        }
    }
    
    /*
        Returns a label node setup with the text and position desired
     */
    private func setupLabelNode(text: String, position: CGPoint) -> SKLabelNode {
        let labelNode = SKLabelNode(fontNamed: "Starjedi")
        labelNode.position = position
        labelNode.text = text
        return labelNode
    }
    
    /*
        Called as soon as the view is displayed - sets up gamecenter
     */
    override func didMove(to view: SKView) {
        if !GameKitHelper.sharedInstance.isAccessPointCreated() {
            GameKitHelper.sharedInstance.authenticatePlayer(mainView: self.view!)
        }
        GameKitHelper.sharedInstance.showAccessPoint()
    }
}
