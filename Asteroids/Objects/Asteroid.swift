//
//  Asteroid.swift
//  Asteroids
//
//  Created by Michael Shepherd on 17/02/2021.
//
//  Handles the Asteroid objects that fall from the sky and attempt to hit the player
//

import Foundation
import SpriteKit

class Asteroid: Object {
    
    private var velX: CGFloat = 0
    private var velY: CGFloat = 0
    private var shouldReset: Bool = false
    private var currentSpeed: CGPoint
    
    init(screenSize: CGSize, position: CGPoint = CGPoint(x: 0, y: 0), size: CGSize = Constants.ASTEROID_SIZE, startSpeed: CGPoint = Constants.BASE_ASTEROID_SPEED) {
        self.currentSpeed = startSpeed
        super.init(position: position, texture: SKTexture(imageNamed: "Asteroid"), size: size)
        
        self.name = "Asteroid"
        self.physicsBody!.categoryBitMask = CollisionMask.Enemy.rawValue
        self.physicsBody!.contactTestBitMask = CollisionMask.Friendly.rawValue
        self.physicsBody!.collisionBitMask = CollisionMask.Enemy.rawValue
        
        setRandomPosition(screenSize: screenSize)
        setupVelocity()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Gets a random starting position for the Asteroid
     */
    private func setRandomPosition(screenSize: CGSize) {
        let randomX = CGFloat.random(in: (Constants.ASTEROID_SIZE.width / 2)...(screenSize.width - Constants.ASTEROID_SIZE.width / 2))
        let randomY = screenSize.height + CGFloat.random(in: (Constants.ASTEROID_SIZE.height / 2)...(screenSize.height - Constants.ASTEROID_SIZE.height / 2))
        self.position = CGPoint(x: randomX, y: randomY)
    }
    
    /*
        Gets a random velocity for the asteroid to move at
     */
    private func setupVelocity() {
        let xMultiplier = CGFloat.random(in: 0.5...1.5)
        let xNegativeOrPositive = Int.random(in: 0...1)
        let yMultiplier = CGFloat.random(in: 0.5...1.5)
        if xNegativeOrPositive == 0 {
            velX = -currentSpeed.x * xMultiplier
        } else {
            velX = currentSpeed.x * xMultiplier
        }
        velY = -currentSpeed.y * yMultiplier
    }
    
    /*
        Updates the Asteroid ever tick
     */
    public func update(screenSize: CGSize, hud: HUD) {
        if shouldReset {
            self.reset(screenSize: screenSize)
            shouldReset = false
        }
        
        position.x += velX
        position.y += velY
        
        checkBounds(screenSize: screenSize, hud: hud)
    }
    
    /*
        Check whether the asteroid has left the bounds of the screen and if so resets its position and velocity
     */
    private func checkBounds(screenSize: CGSize, hud: HUD) {
        if position.y < -Constants.ASTEROID_SIZE.height - 20 {
            self.reset(screenSize: screenSize)
            hud.increaseScore(scoreIncrease: Constants.SCORE_INCREMENT / 2)
        }
    }
    
    /*
        Called when the asteroid gets destroying and handles animating the explosion on impact
     */
    public func handleDestruction(hud: HUD) {
        if let explosion = SKEmitterNode(fileNamed: "Explosion") {
            self.addChild(explosion)
        }
        self.velX = 0
        self.velY = 0
        let soundEffect = SKAction.playSoundFileNamed("explosion.wav", waitForCompletion: false)
        
        let waitBlock = SKAction.wait(forDuration: 0.4)
        let runBlock = SKAction.run {
            self.setShouldReset(shouldReset: true)
            self.removeAllChildren()
            hud.increaseScore()
        }
        self.run(SKAction.sequence([soundEffect, waitBlock, runBlock]))
    }
    
    /*
        Resets the asteroid back to a random starting position and velocity
     */
    public func reset(screenSize: CGSize) {
        self.setRandomPosition(screenSize: screenSize)
        setupVelocity()
    }
    
    /*
        Handles whether the asteroid should be reset
     */
    public func setShouldReset(shouldReset: Bool) {
        self.shouldReset = shouldReset
    }
    
    /*
        Increases the speed of the asteroid by a set increment unless it already equals the max speed
     */
    public func increaseSpeed() {
        currentSpeed.y += Constants.ASTEROID_SPEED_INCREASE
        if currentSpeed.y >= Constants.MAX_ASTEROID_Y_SPEED {
            currentSpeed.y = Constants.MAX_ASTEROID_Y_SPEED
        }
    }
    
}
