//
//  Ammo.swift
//  Asteroids
//
//  Created by Michael Shepherd on 28/02/2021.
//
//  Handles the creation of a collectable object - Heart container or ammo
//

import Foundation
import SpriteKit

enum CollectableType {
    case Ammo
    case Heart
}

class Collectable: Object {
    private var velX: CGFloat
    private var velY: CGFloat
    private var previousScoreCheckedFor: Int
    private let increase: Int
    private var shouldReset: Bool
    private let type: CollectableType
    private let spawnRate: Int
    
    /*
        Initializes all the Ammo properties
     */
    init(screenSize: CGSize, increase: Int = Constants.AMMO_INCREASE, type: CollectableType, position: CGPoint = CGPoint(x: 0, y: 0), size: CGSize = Constants.COLLECTABLE_SIZE) {
        self.velX = 0
        self.velY = 0
        self.type = type
        self.previousScoreCheckedFor = 0
        if type == CollectableType.Ammo {
            self.increase = increase
            self.spawnRate = 200
        } else {
            self.increase = 1
            self.spawnRate = 500
        }
        self.shouldReset = false
        
        if type == CollectableType.Ammo {
            super.init(position: position, texture: SKTexture(imageNamed: "Ammo"), size: size)
        } else {
            super.init(position: position, texture: SKTexture(imageNamed: "Lives"), size: size)
        }
        
        self.name = "Collectable"
        self.physicsBody!.categoryBitMask = CollisionMask.Friendly.rawValue
        self.physicsBody!.contactTestBitMask = CollisionMask.Friendly.rawValue
        self.physicsBody!.collisionBitMask = CollisionMask.Nothing.rawValue
        
        self.reset(screenSize: screenSize)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Updates the ammo every frame
     */
    public func update(screenSize: CGSize, currentScore: Int) {
        if currentScore / spawnRate > previousScoreCheckedFor {
            previousScoreCheckedFor = currentScore / spawnRate
            let shouldSpawn = Int.random(in: 1...5)
            if shouldSpawn == 1 {
                self.spawn()
            }
        }
        
        if shouldReset {
            reset(screenSize: screenSize)
            shouldReset = false
        }
        
        position.x += velX
        position.y += velY
        
        //Reset ammo if it goes off the screen
        if position.y < -size.height {
            reset(screenSize: screenSize)
        }
    }
    
    /*
        Resets the ammo to a random start x and y and sets the velocity to 0
     */
    public func reset(screenSize: CGSize) {
        let randomX = CGFloat.random(in: (Constants.COLLECTABLE_SIZE.width)...(screenSize.width - Constants.COLLECTABLE_SIZE.width))
        let randomY = screenSize.height + CGFloat.random(in: (Constants.COLLECTABLE_SIZE.height / 2)...(screenSize.height - Constants.COLLECTABLE_SIZE.height / 2))
        self.position = CGPoint(x: randomX, y: randomY)
        
        self.velX = 0
        self.velY = 0
    }
    
    /*
        Sets up the velocities so that the ammo object can move onto the screen
     */
    private func spawn() {
        let xNegativeOrPositive = Int.random(in: 0...1)
        if xNegativeOrPositive == 0 {
            velX = -Constants.COLLECTABLE_SPEED.x
        } else {
            velX = Constants.COLLECTABLE_SPEED.x
        }
        velY = -Constants.COLLECTABLE_SPEED.y
    }
    
    /*
        Returns the amount that this ammo object should increase the ammo by
     */
    public func getIncrease() -> Int {
        return increase
    }
    
    /*
        Sets whether the ammo object should get reset
     */
    public func setShouldReset(shouldReset: Bool) {
        self.shouldReset = shouldReset
    }
    
    /*
        Returns the type of the collectable object
     */
    public func getType() -> CollectableType {
        return type
    }
}
