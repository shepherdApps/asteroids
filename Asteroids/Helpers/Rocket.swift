//
//  Rocket.swift
//  Asteroids
//
//  Created by Michael Shepherd on 24/02/2021.
//
//  Handles all the code for loading the rocket and setting up the rocket images
//

import Foundation
import SpriteKit

/*
    Struct containing all the information needed to represent a rocket
 */
struct Rocket: Codable {
    public let name: String
    public let bodyColorString: String
    public let standColorString: String
    public let windowColorString: String
    public let lives: Int
    public let ammoCapacity: Int
    public var unlocked: Bool
    public let unlockCost: Int
}

/*
    Setup all the parts of the rocket image with all the correct colors
 */
func setupRocketImageComponents(rocketNode: SKSpriteNode, baseColor: SKColor, standColor: SKColor, windowColor: SKColor, scale: CGFloat = 1) {
    let baseImage = SKTexture(imageNamed: "Spaceship")
    let standsImage = SKSpriteNode(imageNamed: "shipStands")
    let windowImage = SKSpriteNode(imageNamed: "Window")
    
    rocketNode.texture = baseImage
    rocketNode.color = baseColor
    rocketNode.colorBlendFactor = 1.0
    
    standsImage.color = standColor
    standsImage.colorBlendFactor = 1.0
    standsImage.position = CGPoint(x: 0, y: -rocketNode.size.height / 2 + standsImage.size.height / 2)
    standsImage.name = "Stands"
    rocketNode.addChild(standsImage)
    
    windowImage.color = windowColor
    windowImage.colorBlendFactor = 1.0
    windowImage.position = CGPoint(x: -0.5, y: 7)
    windowImage.name = "Window"
    rocketNode.addChild(windowImage)
}

/*
    Converts a hex color string red from the JSON file into a CGColor
 */
func getColorFromHexString(colorString: String) -> UIColor {
    let hexColor = colorString.dropFirst()
    if let rgbValue = UInt(hexColor, radix: 16) {
        let red = CGFloat((rgbValue >> 16) & 0xff) / 255
        let green = CGFloat((rgbValue >> 8) & 0xff) / 255
        let blue = CGFloat((rgbValue) & 0xff) / 255
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    return UIColor.white
}

/*
    Get the location of the apps documents directory which can be used for reading and writing files
 */
func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}

/*
    Creates the array of Rockets objects and stores in json file in documents so can be re-written to
 */
func createRocketsArrayAndStore() {
    let data = createAndEncodeRocketsArray()
    storeRocketJSONInFile(data: data)
}

/*
    Creates the inital rockets array and encodes into JSON so that it can be written to file
 */
func createAndEncodeRocketsArray() -> Data {
    var rockets: [Rocket] = []
    
    rockets.append(Rocket(name: "Scrap Metal", bodyColorString: "#0000FF", standColorString: "#FF0000", windowColorString: "#D3D3D3", lives: 3, ammoCapacity: 10, unlocked: true, unlockCost: 0))
    rockets.append(Rocket(name: "Tin Can", bodyColorString: "#708090", standColorString: "#006400", windowColorString: "#02075D", lives: 4, ammoCapacity: 12, unlocked: false, unlockCost: 100))
    rockets.append(Rocket(name: "one Small Step", bodyColorString: "#00FF00", standColorString: "#FFFFFF", windowColorString: "#000000", lives: 5, ammoCapacity: 14, unlocked: false, unlockCost: 100))
    rockets.append(Rocket(name: "one Giant Leap", bodyColorString: "#6A0DAD", standColorString: "#40EDD0", windowColorString: "#964B00", lives: 6, ammoCapacity: 18, unlocked: false, unlockCost: 100))
    
    do {
        let encoder = JSONEncoder()
        return try encoder.encode(rockets)
    } catch {
        fatalError("Could not convert rocket array to JSON\n\(error)")
    }
}

/*
    Takes in the Rockets json object and writes to file
 */
func storeRocketJSONInFile(data: Data) {
    let url = getDocumentsDirectory().appendingPathComponent(Constants.ROCKET_JSON_FILE)
    do {
        try data.write(to: url)
    } catch {
        fatalError("Could not write JSON object to file\n\(error)")
    }
}

/*
    Loads the rocket data from the JSON file and converts to a Rocket array
 */
func loadRocketsFromJSON() -> [Rocket] {
    let url = getDocumentsDirectory().appendingPathComponent(Constants.ROCKET_JSON_FILE)
    do {
        let data = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        let rockets = try decoder.decode([Rocket].self, from: data)
        return rockets
    } catch {
        fatalError("Could not load JSON Data from File\n\(error)")
    }
}
