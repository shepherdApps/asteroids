//
//  Bullet.swift
//  Asteroids
//
//  Created by Michael Shepherd on 17/02/2021.
//
//  Class for the Bullet object that a player or enemy can fire
//

import Foundation
import SpriteKit

class Bullet: Object {
    
    private var fired: Bool = false
    private var velY: CGFloat = 0
    private let fromEnemy: Bool

    init(position: CGPoint, color: UIColor = .green, fromEnemy: Bool = false, size: CGSize = Constants.BULLET_SIZE){
        self.fromEnemy = fromEnemy
        super.init(position: position, color: color, size: size)
        self.name = "Bullet"
        
        if !fromEnemy {
            self.physicsBody!.categoryBitMask = CollisionMask.Friendly.rawValue
            self.physicsBody!.contactTestBitMask = CollisionMask.Enemy.rawValue
        }
        else {
            self.physicsBody!.categoryBitMask = CollisionMask.Enemy.rawValue
            self.physicsBody!.contactTestBitMask = CollisionMask.Friendly.rawValue
        }
        self.physicsBody!.collisionBitMask = CollisionMask.Nothing.rawValue
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Updates the bullet every frame
     */
    public override func update(screenSize: CGSize) {
        self.position.y += velY
        if fired {
            self.checkBounds(screenSize: screenSize)
        }
    }
    
    /*
        Update the fired property and handle setting the velocity if the bullet has been fired
     */
    public func setFired(fired: Bool) {
        self.fired = fired
        
        if fired {
            let soundEffect = SKAction.playSoundFileNamed("bullet_firing.wav", waitForCompletion: false)
            self.run(soundEffect)
        }
        
        if fired && !fromEnemy {
            velY = Constants.BULLET_SPEED
        }
        else if fired {
            velY = -Constants.BULLET_SPEED
        }
        else {
            velY = 0
        }
    }
    
    /*
        Returns the fired property
     */
    public func getFired() -> Bool {
        return fired
    }
    
    /*
        Check if the bullet has left the bounds of the screen and handle accordingly
     */
    private func checkBounds(screenSize: CGSize) {
        if position.y >= screenSize.height + self.size.height / 2 {
            self.setFired(fired: false)
        }
        else if position.y <= -self.size.height / 2 {
            self.setFired(fired: false)
        }
    }
    
    /*
        Returns whether the bullet was created by an enemy or the player
     */
    public func getFromEnemy() -> Bool {
        return fromEnemy
    }
}
