//
//  GameKitHelper.swift
//  Asteroids
//
//  Created by Michael Shepherd on 07/03/2021.
//
//  Handles the GameKit code for allowing integration with GameCenter
//

import Foundation
import GameKit

/*
    Class to handle all the gameCenter integration code
 */
class GameKitHelper: NSObject, GKGameCenterControllerDelegate {
    
    private static let singleton = GameKitHelper()
    private var loadedAchievements: [GKAchievement]
    private var updatedAchievements: [GKAchievement]
    private var accessPointCreated: Bool
    
    override private init() {
        self.loadedAchievements = []
        self.updatedAchievements = []
        self.accessPointCreated = false
    }
    
    /*
        Required - dismiss view when finished
     */
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
    /*
        Handles authenticating the users gameCenter account
     */
    public func authenticatePlayer(mainView: SKView) {
        GKLocalPlayer.local.authenticateHandler = { view, error in
            if view != nil {
                mainView.window!.rootViewController!.present(view!, animated: true, completion: {
                    self.createAccessPoint()
                })
            } else if error != nil {
                print("Game center not authorized")
                print(error.debugDescription)
            } else {
                self.createAccessPoint()
            }
        }
    }
    
    /*
        Resets all the current users achievements
     */
    public func resetAchievements() {
        if GKLocalPlayer.local.isAuthenticated {
            GKAchievement.resetAchievements(completionHandler: {(error: Error?) -> Void in
                if error != nil {
                    print("Unable to reset achievements")
                    print(error!.localizedDescription)
                } else {
                    print("Achievements reset")
                }
            })
        }
    }
    
    /*
        Creates the access point that allows the user to view the gameCenter information panels
     */
    public func createAccessPoint() {
        if GKLocalPlayer.local.isAuthenticated {
            GKAccessPoint.shared.location = .topLeading
            GKAccessPoint.shared.showHighlights = true
            GKAccessPoint.shared.isActive = true
        }
        self.accessPointCreated = true
    }
    
    /*
        Removes the gameCenter access point from the screen
     */
    public func hideAccessPoint() {
        if GKLocalPlayer.local.isAuthenticated {
            GKAccessPoint.shared.isActive = false
        }
    }
    
    /*
        Adds the gamecenter access point to the screen
     */
    public func showAccessPoint() {
        self.loadAchievements()
        if GKLocalPlayer.local.isAuthenticated {
            GKAccessPoint.shared.isActive = true
        }
    }
    
    /*
        Loads all the achievements that have been previously reported on
     */
    public func loadAchievements() {
        if GKLocalPlayer.local.isAuthenticated {
            GKAchievement.loadAchievements(completionHandler: { (loadedAchievements, error) -> Void in
                if loadedAchievements != nil {
                    self.loadedAchievements = loadedAchievements!
                }
                if error != nil {
                    fatalError("Something went wrong with loading the achievements from GameCenter")
                }
            })
        }
    }
    
    /*
        If there is an achievement already stored with the identifier given, returns that achievement, otherwise returns nil
     */
    public func getAchievementFromArray(identifier: String) -> GKAchievement? {
        let filteredArray = loadedAchievements.filter ({ $0.identifier == identifier })
        if filteredArray.count != 0 {
            return filteredArray[0]
        }
        return nil
    }
    
    /*
        Report all the achievements that have been gained through the playing
     */
    public func reportAchievements() {
        print("Reporting achievements")
        if GKLocalPlayer.local.isAuthenticated {
            print("Achievements being reported: ")
            for achievement in updatedAchievements {
                print(achievement.identifier)
            }
            GKAchievement.report(self.updatedAchievements, withCompletionHandler: { (error: Error?) -> Void in
                if error != nil {
                    print("An error has occurred when reporting gained achievements to gamecenter")
                    print(error!.localizedDescription)
                }
            })
            
            for achievement in updatedAchievements {
                let achievementStored = loadedAchievements.filter({ $0.identifier == achievement.identifier })
                if achievementStored.count == 0 {
                    loadedAchievements.append(achievement)
                }
                else {
                    loadedAchievements.remove(at: loadedAchievements.firstIndex(of: achievementStored[0])!)
                    loadedAchievements.append(achievement)
                }
            }
            updatedAchievements.removeAll()
        }
    }
    
    /*
        Creates the object for a given achievement based on the passed in identifier
     */
    public func createAchievementObject(identifier: String) -> GKAchievement {
        let achievement: GKAchievement? = GKAchievement(identifier: identifier)
        if achievement == nil {
            fatalError("Issue with finding achievement with ID: \(identifier)")
        }
        
        return achievement!
    }
    
    /*
        Adds an achievement to the achievements list so that it can get reported to GameCenter
     */
    public func addAchievement(progressedAchievement: GKAchievement) {
        updatedAchievements.append(progressedAchievement)
    }
    
    /*
        Returns whether the game center access point has been created for the current game session
     */
    public func isAccessPointCreated() -> Bool {
        return accessPointCreated
    }
    
    /*
        Provide access to the shared instance of this singleton class
     */
    class var sharedInstance: GameKitHelper { return singleton }
}
