//
//  Object.swift
//  Asteroids
//
//  Created by Michael Shepherd on 17/02/2021.
//
//  Base Class for all Objects used in the game
//

import Foundation
import SpriteKit

/*
    Stores the different collision masks that can be used by the different objects in the game
 */
enum CollisionMask: UInt32 {
    case Friendly = 0b0001
    case Enemy = 0b0010
    case Nothing = 0b0000
    case Everything = 0b0011
}

class Object: SKSpriteNode {
    
    init(position: CGPoint, color: UIColor, size: CGSize) {
        super.init(texture: nil, color: color, size: size)
        setupPositionAndPhysics(position: position)
    }
    
    init(position: CGPoint, texture: SKTexture, size: CGSize) {
        super.init(texture: texture, color: .white, size: size)
        setupPositionAndPhysics(position: position)
    }
    
    /*
        Sets up how the object should interact to the world physics
     */
    private func setupPositionAndPhysics(position: CGPoint) {
        self.position = position
        self.physicsBody = SKPhysicsBody(rectangleOf: size)
        self.physicsBody!.isDynamic = true
        self.physicsBody!.affectedByGravity = false
    }
    
    /*
        Performs an animation when a object takes damage
     */
    public func takeDamage() {
        let flashTransparency = SKAction.fadeAlpha(to: 0.0, duration: 0.2)
        let returnToNormal = SKAction.fadeAlpha(to: 1.0, duration: 0.2)
        let actionChain = SKAction.sequence([flashTransparency, returnToNormal])
        self.run(actionChain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Updates the object every frame - must be implemented by all sub-classes
     */
    public func update(screenSize: CGSize) {
        fatalError("Update function has not been overridden in base class")
    }
}
