//
//  GameOverScene.swift
//  Asteroids
//
//  Created by Michael Shepherd on 21/02/2021.
//
//  Displays the Game Over screen showing the user their score and letting the play again if wanted
//

import Foundation
import SpriteKit
import GameKit

class GameOverScene: SKScene {
    private var background: SKSpriteNode
    private var gameOverLabel: SKLabelNode = SKLabelNode()
    private var scoreLabel: SKLabelNode = SKLabelNode()
    private var playAgainLabel: SKLabelNode = SKLabelNode()
    private var exitLabel: SKLabelNode = SKLabelNode()
    
    private var finalScore: Int
    private let screenSize: CGSize
    
    override init(size: CGSize) {
        self.screenSize = size
        let backgroundTexture = SKTexture(imageNamed: "Background")
        background = SKSpriteNode(texture: backgroundTexture)
        finalScore = 0
        super.init(size: size)
        
        background.size = CGSize(width: size.width, height: size.height * 2)
        background.position = CGPoint(x: size.width / 2, y: size.height)
        self.addChild(background)
        
        gameOverLabel = setupLabelNode(text: "Game over", position: CGPoint(x: size.width / 2, y: size.height / 2 + 150))
        scoreLabel = setupLabelNode(text: "Final Score: " + String(finalScore), position: CGPoint(x: size.width / 2, y: size.height / 2 + 75))
        playAgainLabel = setupLabelNode(text: "Play Again", position: CGPoint(x: size.width / 2, y: size.height / 2 - 50))
        exitLabel = setupLabelNode(text: "Exit", position: CGPoint(x: size.width / 2, y: size.height / 2 - 125))
        
        gameOverLabel.fontColor = .green
        self.addChild(gameOverLabel)
        self.addChild(scoreLabel)
        self.addChild(playAgainLabel)
        self.addChild(exitLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Called when the user presses on the screen - handles all button input
    */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        let location = touch.location(in: self)
        let revealTransition = SKTransition.reveal(with: .right, duration: 1)
        if playAgainLabel.frame.contains(location) {
            self.view?.presentScene(RocketScene(size: screenSize), transition: revealTransition)
        }
        else if exitLabel.frame.contains(location) {
            self.view?.presentScene(MenuScene(screenSize: screenSize), transition: revealTransition)
        }
    }
    
    /*
        Sets up all the common properties of a labelNode
     */
    private func setupLabelNode(text: String, position: CGPoint) -> SKLabelNode {
        let labelNode = SKLabelNode(fontNamed: "Starjedi")
        labelNode.position = position
        labelNode.text = text
        return labelNode
    }
    
    /*
        Set the score that the user finished the game with
     */
    public func setFinalScore(score: Int) {
        self.finalScore = score
        scoreLabel.text = "Final Score: " + String(self.finalScore)
        
        let userTotalFunds = UserDefaults.standard.integer(forKey: "money") + self.finalScore
        UserDefaults.standard.set(userTotalFunds, forKey: "money")
        
        self.checkAchievements()
    }
    
    /*
        Checks all the achievements that could have been unlocked during the course of the game
     */
    private func checkAchievements() {
        if GKLocalPlayer.local.isAuthenticated {
            if self.finalScore >= 100 {
                var liftOffAchievement: GKAchievement? = GameKitHelper.sharedInstance.getAchievementFromArray(identifier: "LIFT_OFF")
                // If the achievement has not previously been obtained
                if liftOffAchievement == nil {
                    liftOffAchievement = GameKitHelper.sharedInstance.createAchievementObject(identifier: "LIFT_OFF")
                    liftOffAchievement!.percentComplete = 100
                    liftOffAchievement!.showsCompletionBanner = true
                    GameKitHelper.sharedInstance.addAchievement(progressedAchievement: liftOffAchievement!)
                    GameKitHelper.sharedInstance.reportAchievements()
                }
            }
        }
    }
}
