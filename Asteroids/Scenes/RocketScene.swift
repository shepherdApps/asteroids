//
//  RocketScene.swift
//  Asteroids
//
//  Created by Michael Shepherd on 24/02/2021.
//
//  Scene where we can select the rocket that we want to use in the game - only can select unlocked rockets
//

import Foundation
import SpriteKit
import GameKit

class RocketScene: SKScene {
    private var background: SKSpriteNode = SKSpriteNode()
    
    private var titleLabel: SKLabelNode = SKLabelNode()
    private var rocketImage: SKSpriteNode = SKSpriteNode()
    private var leftArrow: SKSpriteNode = SKSpriteNode()
    private var rightArrow: SKSpriteNode = SKSpriteNode()
    private var nameLabel: SKLabelNode = SKLabelNode()
    private var livesLabel: SKLabelNode = SKLabelNode()
    private var ammoLabel: SKLabelNode = SKLabelNode()
    private var selectButton: SKLabelNode = SKLabelNode()
    private var backButton: SKLabelNode = SKLabelNode()
    
    private var lockedLabel: SKLabelNode = SKLabelNode()
    private var purchaseButton: SKLabelNode = SKLabelNode()
    private var moneyLabel: SKLabelNode = SKLabelNode()
    private var coinImage: SKSpriteNode = SKSpriteNode()
    
    private var rockets: [Rocket]
    private var currentRocketIndex: Int
    private var currentMoney: Int
    
    /*
        Sets up the enitre rocket scene
     */
    override init(size: CGSize) {
        rockets = loadRocketsFromJSON()
        currentRocketIndex = 0
        currentMoney = UserDefaults.standard.integer(forKey: "money")
        
        super.init(size: size)
        
        setupLabels()
        setupImages()
        addAllChildrenToScene()
        
        lockedLabel.isHidden = true
        purchaseButton.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Sets up all the images used in the scene
     */
    private func setupImages() {
        background = SKSpriteNode(imageNamed: "Background")
        background.size = CGSize(width: size.width, height: size.height * 2)
        background.position = CGPoint(x: size.width / 2, y: size.height)
        background.zPosition = -1
        
        rocketImage = SKSpriteNode(texture: nil, color: .white, size: CGSize(width: Constants.PLAYER_SIZE.width, height: Constants.PLAYER_SIZE.height))
        setupRocketImageComponents(rocketNode: rocketImage, baseColor: getColorFromHexString(colorString: rockets[0].bodyColorString),
                                                            standColor: getColorFromHexString(colorString: rockets[0].standColorString),
                                                            windowColor: getColorFromHexString(colorString: rockets[0].windowColorString))
        rocketImage.position = CGPoint(x: size.width / 2, y: size.height / 2 + 100)
        rocketImage.setScale(4)
        
        leftArrow = SKSpriteNode(imageNamed: "Arrow")
        leftArrow.colorBlendFactor = 1.0
        leftArrow.size = Constants.ARROW_SIZE
        leftArrow.position = CGPoint(x: 50, y: size.height / 2 + 100)
        if currentRocketIndex == 0 {
            leftArrow.color = .lightGray
        } else {
            leftArrow.color = .green
        }
        
        rightArrow = SKSpriteNode(imageNamed: "Arrow")
        rightArrow.colorBlendFactor = 1.0
        rightArrow.xScale = rightArrow.xScale * -1
        rightArrow.position = CGPoint(x: size.width - 50, y: size.height / 2 + 100)
        if currentRocketIndex == rockets.count - 1 {
            rightArrow.color = .lightGray
        } else {
            rightArrow.color = .green
        }
        
        coinImage = SKSpriteNode()
        coinImage.texture = SKTexture(image: UIImage(named: "Coin")!)
        coinImage.size = CGSize(width: 30, height: 30)
        coinImage.position = CGPoint(x: size.width - 30 - moneyLabel.frame.width - 25, y: moneyLabel.position.y + 10)
    }
    
    /*
        Sets up all the label nodes used in the scene
     */
    private func setupLabels() {
        titleLabel = setupLabelNode(text: "Select Rocket", position: CGPoint(x: size.width / 2, y: size.height - 125))
        nameLabel = setupLabelNode(text: "\(rockets[currentRocketIndex].name)", position: CGPoint(x: size.width / 2, y: size.height / 2 - 100))
        livesLabel = setupLabelNode(text: "Lives:   \(rockets[currentRocketIndex].lives)", position: CGPoint(x: size.width / 2, y: size.height / 2 - 175))
        ammoLabel = setupLabelNode(text: "Ammo:   \(rockets[currentRocketIndex].ammoCapacity)", position: CGPoint(x: size.width / 2, y: size.height / 2 - 225))
        selectButton = setupLabelNode(text: "use this Rocket", position: CGPoint(x: size.width / 2, y: size.height / 2 - 300), color: .green)
        lockedLabel = setupLabelNode(text: "Locked", position: CGPoint(x: size.width / 2, y: size.height / 2 - 175))
        purchaseButton = setupMultilineLabelNode(text: "unlock for\n\(rockets[currentRocketIndex].unlockCost) coins", position: CGPoint(x: size.width / 2, y: size.height / 2 - 320), color: .green)
        moneyLabel = setupLabelNode(text: "\(currentMoney)", position: CGPoint(x: size.width - 30, y: size.height - 75))
        moneyLabel.position.x = size.width - 30 - moneyLabel.frame.width / 2
        backButton = setupLabelNode(text: "Back", position: CGPoint(x: 20, y: size.height - 75), color: .systemBlue)
        backButton.position.x = 20 + backButton.frame.width / 2
    }
    
    /*
        Returns a label node setup with the text and position desired
     */
    private func setupLabelNode(text: String, position: CGPoint, color: UIColor = .white) -> SKLabelNode {
        let labelNode = SKLabelNode(fontNamed: "Starjedi")
        labelNode.fontSize = 30
        labelNode.fontColor = color
        labelNode.text = text
        labelNode.position = position
        return labelNode
    }

    /*
        Handles a multiline label - needs to use attributed string so that we can center the text on both lines
        Used for the purchase button
     */
    private func setupMultilineLabelNode(text: String, position: CGPoint, color: UIColor = .white) -> SKLabelNode {
        let labelNode = SKLabelNode(fontNamed: "Starjedi")
        
        labelNode.position = position
        labelNode.numberOfLines = 2
        
        var displayedColor = color
        if currentMoney < rockets[currentRocketIndex].unlockCost {
            displayedColor = .gray
        }
        
        let attrString = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let range = NSRange(location: 0, length: text.count)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: range)
        attrString.addAttributes([NSAttributedString.Key.foregroundColor : displayedColor, NSAttributedString.Key.font : UIFont(name: "Starjedi", size: 30)!], range: range)
        labelNode.attributedText = attrString
        
        return labelNode
    }
    
    /*
        Adds all the child nodes to the scene
     */
    private func addAllChildrenToScene() {
        self.addChild(background)
        self.addChild(titleLabel)
        self.addChild(rocketImage)
        self.addChild(leftArrow)
        self.addChild(rightArrow)
        self.addChild(nameLabel)
        self.addChild(livesLabel)
        self.addChild(ammoLabel)
        self.addChild(selectButton)
        self.addChild(lockedLabel)
        self.addChild(purchaseButton)
        self.addChild(moneyLabel)
        self.addChild(coinImage)
        self.addChild(backButton)
    }
    
    /*
        Called when a touch is done on the scene - handle all user input for the scene
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        let location = touch.location(in: self)
        
        if rightArrow.frame.contains(location) && currentRocketIndex != rockets.count - 1 {
            currentRocketIndex += 1
            refreshSceneForNewRocket()
            
            if currentRocketIndex == 1 {
                leftArrow.color = .green
            }
            
            if currentRocketIndex == rockets.count - 1 {
                rightArrow.color = .lightGray
            }
            
            updateLabelText()
        }
        else if leftArrow.frame.contains(location) && currentRocketIndex != 0 {
            currentRocketIndex -= 1
            refreshSceneForNewRocket()
            
            if currentRocketIndex == rockets.count - 2 {
                rightArrow.color = .green
            }
            
            if currentRocketIndex == 0 {
                leftArrow.color = .lightGray
            }
            
            updateLabelText()
        }
        else if selectButton.frame.contains(location) && !selectButton.isHidden {
            let revealTransition = SKTransition.reveal(with: .left, duration: 1)
            self.view?.presentScene(GameScene(size: self.size, rocket: rockets[currentRocketIndex]), transition: revealTransition)
        }
        else if purchaseButton.frame.contains(location) && !purchaseButton.isHidden {
            if currentMoney >= rockets[currentRocketIndex].unlockCost {
                rockets[currentRocketIndex].unlocked = true
                currentMoney = currentMoney - rockets[currentRocketIndex].unlockCost
                UserDefaults.standard.set(currentMoney, forKey: "money")
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(rockets)
                    Asteroids.storeRocketJSONInFile(data: data)
                } catch {
                    fatalError("Could not write Rockets to JSON file\n\(error)")
                }
                refreshSceneForNewRocket()
                updateLabelText()
                checkAchievementsWhenBoughtRocket()
            }
        }
        else if backButton.frame.contains(location) {
            let revealTransition = SKTransition.reveal(with: .right, duration: 1)
            self.view?.presentScene(MenuScene(screenSize: self.size), transition: revealTransition)
        }
    }
    
    /*
        Updates the view for when the current rocket is changed - changes rocket colours and sets up the correct labels to be displayed
     */
    private func refreshSceneForNewRocket() {
        if rockets[currentRocketIndex].unlocked {
            rocketImage.color = getColorFromHexString(colorString: rockets[currentRocketIndex].bodyColorString)
            (rocketImage.childNode(withName: "Stands") as! SKSpriteNode).color = getColorFromHexString(colorString: rockets[currentRocketIndex].standColorString)
            (rocketImage.childNode(withName: "Window") as! SKSpriteNode).color = getColorFromHexString(colorString: rockets[currentRocketIndex].windowColorString)
            lockedLabel.isHidden = true
            purchaseButton.isHidden = true
            livesLabel.isHidden = false
            ammoLabel.isHidden = false
            selectButton.isHidden = false
        } else {
            rocketImage.color = .gray
            (rocketImage.childNode(withName: "Stands") as! SKSpriteNode).color = .lightGray
            (rocketImage.childNode(withName: "Window") as! SKSpriteNode).color = .lightGray
            lockedLabel.isHidden = false
            purchaseButton.isHidden = false
            livesLabel.isHidden = true
            ammoLabel.isHidden = true
            selectButton.isHidden = true
        }
    }
    
    private func updateLabelText() {
        nameLabel.text = "\(rockets[currentRocketIndex].name)"
        livesLabel.text = "Lives:   \(rockets[currentRocketIndex].lives)"
        moneyLabel.text = "\(currentMoney)"
        moneyLabel.position.x = size.width - 30 - moneyLabel.frame.width / 2
        self.removeChildren(in: [purchaseButton])
        purchaseButton = setupMultilineLabelNode(text: "unlock for\n\(rockets[currentRocketIndex].unlockCost) coins", position: CGPoint(x: size.width / 2, y: size.height / 2 - 320), color: .green)
        if rockets[currentRocketIndex].unlocked {
            purchaseButton.isHidden = true
        }
        self.addChild(purchaseButton)
    }
    
    /*
        Checks whether the player has unlocked an achievement when they perform an action
     */
    private func checkAchievementsWhenBoughtRocket() {
        if GKLocalPlayer.local.isAuthenticated {
            let improvementsAchievement: GKAchievement = GameKitHelper.sharedInstance.createAchievementObject(identifier: "FIRST_ROCKET")
            let rocketScienceAchievement: GKAchievement = GameKitHelper.sharedInstance.createAchievementObject(identifier: "ROCKET_SCIENCE")
            
            if !improvementsAchievement.isCompleted {
                improvementsAchievement.percentComplete = 100
                improvementsAchievement.showsCompletionBanner = true
                rocketScienceAchievement.showsCompletionBanner = true
                GameKitHelper.sharedInstance.addAchievement(progressedAchievement: improvementsAchievement)
                GameKitHelper.sharedInstance.reportAchievements()
            }
            
            if !rocketScienceAchievement.isCompleted {
                rocketScienceAchievement.percentComplete = rocketScienceAchievement.percentComplete + 10.0
                print("New Percentage complete: " + String(rocketScienceAchievement.percentComplete))
                GameKitHelper.sharedInstance.addAchievement(progressedAchievement: rocketScienceAchievement)
                GameKitHelper.sharedInstance.reportAchievements()
            }
        }
    }
    
    /*
        Called when the view is displayed - hides the gamecenter access point
     */
    override func didMove(to view: SKView) {
        GameKitHelper.sharedInstance.hideAccessPoint()
    }

}
