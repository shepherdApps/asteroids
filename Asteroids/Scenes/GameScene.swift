//
//  GameScene.swift
//  Asteroids
//
//  Created by Michael Shepherd on 15/02/2021.
//
//  Handles the main game scene - all the actual gameplay is handled here
//

import Foundation
import SpriteKit
import SwiftUI

class GameScene: SKScene, SKPhysicsContactDelegate {
    private var player: Player
    private var asteroids : [Asteroid] = []
    private var enemies: [Enemy] = []
    private var hud: HUD
    private var gameOver: Bool
    private var ammoCollectable: Collectable
    private var heartCollectable: Collectable
    private var draggingTouch: Bool = false
    private var previousHandledScoreIncrement: Int
    private var previouslyAddedEnemy: Bool
    
    //Will always contain 2 elements
    private var backgrounds: [SKSpriteNode] = []
    
    init(size: CGSize, rocket: Rocket) {
        self.player = Player(position: CGPoint(x: size.width / 2, y: 100), rocket: rocket, screenSize: size)
        self.ammoCollectable = Collectable(screenSize: size, type: CollectableType.Ammo)
        self.heartCollectable = Collectable(screenSize: size, type: CollectableType.Heart)
        self.hud = HUD(screenSize: size)
        self.gameOver = false
        self.previousHandledScoreIncrement = 0
        self.previouslyAddedEnemy = false
        super.init(size: size)
        self.createBackground()
        
        for _ in 1...Constants.STARTING_NUM_ASTEROIDS {
            let asteroid = Asteroid(screenSize: size)
            asteroids.append(asteroid)
            self.addChild(asteroid)
        }
        
        player.addChildrenToScene(scene: self)
        hud.addObjectsToScene(scene: self)
        self.addChild(player)
        self.addChild(ammoCollectable)
        self.addChild(heartCollectable)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Setup the scrolling backgrounds in order to make it look like the background is always moving
     */
    private func createBackground() {
        let backgroundTexture = SKTexture(imageNamed: "Background")
        let backgroundShift = SKAction.repeatForever(SKAction.moveBy(x: 0, y: -size.height * 2, duration: 20))
        backgrounds = [SKSpriteNode(texture: backgroundTexture), SKSpriteNode(texture: backgroundTexture)]
        
        for i in 0...(backgrounds.count - 1) {
            backgrounds[i].position = CGPoint(x: self.size.width / 2, y: self.size.height + CGFloat(i) * self.size.height * 2 - 20)
            backgrounds[i].size = CGSize(width: self.size.width, height: self.size.height * 2)
            backgrounds[i].run(backgroundShift)
            backgrounds[i].zPosition = -2
            self.addChild(backgrounds[i])
        }
    }
    
    /*
        Called when there is a recoginised collision between 2 objects - all collision detection handled through here
     */
    func didBegin(_ contact: SKPhysicsContact) {
        if let collision = checkCollisionBetweenTwoNodes(node1Name: "Bullet", node2Name: "Asteroid", contact) {
            handleBulletAsteroidCollision(bullet: collision.nodeA as! Bullet, asteroid:  collision.nodeB as! Asteroid)
        } else if let collision = checkCollisionBetweenTwoNodes(node1Name: "Player", node2Name: "Asteroid", contact) {
            handlePlayerAsteroidCollision(player: collision.nodeA as! Player, asteroid: collision.nodeB as! Asteroid)
        } else if let collision = checkCollisionBetweenTwoNodes(node1Name: "Collectable", node2Name: "Player", contact) {
            handleCollectablePlayerCollision(collectable: collision.nodeA as! Collectable, player: collision.nodeB as! Player)
        } else if let collision = checkCollisionBetweenTwoNodes(node1Name: "Player", node2Name: "Bullet", contact) {
            let bullet = collision.nodeB as! Bullet
            if bullet.getFromEnemy() {
                handlePlayerBulletCollision(player: collision.nodeA as! Player, bullet: bullet)
            }
        } else if let collision = checkCollisionBetweenTwoNodes(node1Name: "Enemy", node2Name: "Asteroid", contact) {
            handleEnemyCollision(enemy: collision.nodeA as! Enemy, object: collision.nodeB as! Asteroid, isObjectBullet: false)
        } else if let collision = checkCollisionBetweenTwoNodes(node1Name: "Enemy", node2Name: "Bullet", contact) {
            let bullet = collision.nodeB as! Bullet
            if !bullet.getFromEnemy() {
                handleEnemyCollision(enemy: collision.nodeA as! Enemy, object: collision.nodeB as! Bullet, isObjectBullet: true)
            }
        }
    }
    
    /*
        Adds an enemy to the enemy array
     */
    private func addEnemy() {
        let newEnemy = Enemy(position: CGPoint(x: size.width / 2 - Constants.ENEMY_SIZE.width / 2, y: size.height + 100), screenSize: size)
        enemies.append(newEnemy)
        self.addChild(newEnemy)
        newEnemy.addChildrenToScene(scene: self)
    }
    
    /*
        Checks to see if the collision happened between a certain 2 object types and returns those objects if there was a collision
     */
    private func checkCollisionBetweenTwoNodes(node1Name: String, node2Name: String, _ contact: SKPhysicsContact) -> (nodeA: Object, nodeB: Object)? {
        if contact.bodyA.node?.name == node1Name && contact.bodyB.node?.name == node2Name {
            return (contact.bodyA.node as! Object, contact.bodyB.node as! Object)
        } else if contact.bodyB.node?.name == node1Name && contact.bodyA.node?.name == node2Name {
            return (contact.bodyB.node as! Object, contact.bodyA.node as! Object)
        }
        
        return nil
    }
    
    /*
        Handle the collision between a bullet and an asteroid
     */
    private func handleBulletAsteroidCollision(bullet: Bullet, asteroid: Asteroid) {
        if asteroid.position.y <= size.height + asteroid.size.height / 2 {
            bullet.setFired(fired: false)
            asteroid.handleDestruction(hud: self.hud)
        }
    }
    
    /*
        Handle the collision between a player and a asteroid
     */
    private func handlePlayerAsteroidCollision(player: Player, asteroid: Asteroid) {
        player.changeLives(increase: false)
        asteroid.setShouldReset(shouldReset: true)
        if player.getLives() == 0 {
            let gameOverScene = GameOverScene(size: size)
            gameOverScene.setFinalScore(score: hud.getScore())
            
            self.removeAllChildren()
            self.removeFromParent()
            
            self.view?.presentScene(gameOverScene)
        }
    }
    
    /*
        Handle the collision between a ammo object and player object
     */
    private func handleCollectablePlayerCollision(collectable: Collectable, player: Player) {
        if collectable.getType() == CollectableType.Ammo {
            player.increaseAmmo(ammo: collectable.getIncrease())
        } else {
            player.changeLives(increase: true)
        }
        collectable.setShouldReset(shouldReset: true)
    }
    
    /*
        Handle enemy bullet and player collision
     */
    private func handlePlayerBulletCollision(player: Player, bullet: Bullet) {
        player.changeLives(increase: false)
        bullet.setFired(fired: false)
    }
    
    /*
        Handle all collisions with enemy - either asteroid or bullet
     */
    private func handleEnemyCollision(enemy: Enemy, object: Object, isObjectBullet: Bool) {
        enemy.decreaseHealth()
    
        //Check if the enemy was killed by Player or Asteroid to work out what the score increase should be
        if enemy.getDead() {
            if isObjectBullet {
                hud.increaseScore(scoreIncrease: Constants.SCORE_INCREMENT * 2)
            }
            else {
                hud.increaseScore()
            }
        }
        
        if isObjectBullet {
            let bullet = object as! Bullet
            bullet.setFired(fired: false)
        }
        else {
            let asteroid = object as! Asteroid
            asteroid.setShouldReset(shouldReset: true)
        }
    }
    
    /*
        Called as soon as view displayed - sets up the view and physics
     */
    override func didMove(to view: SKView) {
        view.isMultipleTouchEnabled = true
        view.showsPhysics = true
        physicsWorld.contactDelegate = self
    }
    
    /*
        Called when the user holds down on the screen - causes player movement
     */
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: self)
        self.draggingTouch = true
        if !hud.isPaused() {
            player.move(xPosition: location.x, screenWidth: size.width)
        }
    }
    
    /*
        Called when the user first presses the screen - used for player bullet firing
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        let location = touch.location(in: self)
        if hud.checkIfButtonTouched(location: location, currentScene: self) { return }
        
        if !hud.isPaused() && draggingTouch {
            player.fireBullet()
        }
    }
    
    /*
        Called whenever a touch event is ended - meaning the user has lifted a finger off the screen
     */
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //If this is the only touch and it is not being dragged and we have not paused, fire bullet
        if event?.allTouches!.count == 1 && !draggingTouch && !hud.isPaused() {
            player.fireBullet()
        }
        //If this is the final touch and we have been dragging, set dragging to false
        else if draggingTouch && event?.allTouches!.count == 1 {
            self.draggingTouch = false
        }
    }
    
    /*
        Updates the scene every frame
     */
    override func update(_ currentTime: TimeInterval) {
        if !hud.isPaused() {
            if backgrounds[0].isPaused {
                for background in backgrounds {
                    background.isPaused = false
                }
            }
            
            for i in 0...(backgrounds.count - 1) {
                if backgrounds[i].position.y <= -size.height {
                    backgrounds[i].position.y = size.height + size.height * 2 - 20
                }
            }
            
            player.update(screenSize: size)
            for asteroid in asteroids {
                asteroid.update(screenSize: size, hud: hud)
            }
            
            ammoCollectable.update(screenSize: self.size, currentScore: hud.getScore())
            heartCollectable.update(screenSize: self.size, currentScore: hud.getScore())
            
            for enemy in enemies {
                enemy.update(screenSize: self.size)
            }
            
            handleScoreIncrements()
            
            
        } else if !backgrounds[0].isPaused {
            for background in backgrounds {
                background.isPaused = true
            }
        }
    }
    
    /*
        Handles all the events that occur after certain score values
     */
    private func handleScoreIncrements() {
        let currentScoreIncrement = (hud.getScore() / 100)
        
        if currentScoreIncrement != self.previousHandledScoreIncrement {
            if currentScoreIncrement % 10 == 0 {
                for asteroid in asteroids {
                    asteroid.increaseSpeed()
                }
            }
            else if currentScoreIncrement % 5 == 0 {
                if previouslyAddedEnemy && asteroids.count != Constants.MAX_NUM_ASTEROIDS {
                    let newAsteroid = Asteroid(screenSize: size)
                    asteroids.append(newAsteroid)
                    self.addChild(newAsteroid)
                } else {
                    self.addEnemy()
                }
                
                previouslyAddedEnemy = !previouslyAddedEnemy
            }
            
            previousHandledScoreIncrement = currentScoreIncrement
        }
    }
}
