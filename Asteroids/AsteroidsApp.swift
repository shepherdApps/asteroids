//
//  AsteroidsApp.swift
//  Asteroids
//
//  Created by Michael Shepherd on 15/02/2021.
//

import SwiftUI

@main
struct AsteroidsApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
