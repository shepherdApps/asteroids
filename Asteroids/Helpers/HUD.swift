//
//  HUD.swift
//  Asteroids
//
//  Created by Michael Shepherd on 20/02/2021.
//
//  Creates the HUD for use with the GameScene
//

import Foundation
import SpriteKit

class HUD {
    private let pauseButton: SKSpriteNode
    private var scoreLabel: SKLabelNode = SKLabelNode()
    private var pausedLabel: SKLabelNode = SKLabelNode()
    private var resumeButton: SKLabelNode = SKLabelNode()
    private var exitButton: SKLabelNode = SKLabelNode()
    private var paused: Bool
    private var score: Int
    private let screenSize: CGSize
    
    /*
        Set up all the labels and different elements of the HUD
     */
    init(screenSize: CGSize) {
        self.screenSize = screenSize
        self.score = 0
        let imageConfig = UIImage.SymbolConfiguration(pointSize: 16, weight: .ultraLight)
        
        //Workaround for changing the color of the pause button as standard method does not work
        let pauseImage = UIImage(systemName: "pause.fill", withConfiguration: imageConfig)!.withTintColor(UIColor.white)
        let pauseImageData = pauseImage.pngData()
        let texture = SKTexture(image: UIImage(data: pauseImageData!)!)
        self.pauseButton = SKSpriteNode(texture: texture)
        
        self.pauseButton.size = CGSize(width: 40, height: 40)
        self.pauseButton.position = CGPoint(x: 30, y: screenSize.height - 80)
        self.pauseButton.color = .white
        self.pauseButton.zPosition = 1
        paused = false
        
        self.scoreLabel = setupLabelNode(text: "Score: " + String(self.score), position: CGPoint(x: 0, y: 0))
        self.pausedLabel = setupLabelNode(text: "Paused", position: CGPoint(x: screenSize.width / 2, y: screenSize.height / 2 + 100))
        self.resumeButton = setupLabelNode(text: "Resume", position: CGPoint(x: screenSize.width / 2, y: screenSize.height / 2))
        self.exitButton = setupLabelNode(text: "Exit", position: CGPoint(x: screenSize.width / 2, y: screenSize.height / 2 - 100))
        
        //Have to update the scoreLabel position once created to account for the bounds of the object
        self.scoreLabel.position = CGPoint(x: screenSize.width - scoreLabel.frame.width / 2 - 20, y: screenSize.height - 90)
        self.pausedLabel.fontColor = .green
        
        self.pausedLabel.isHidden = true
        self.resumeButton.isHidden = true
        self.exitButton.isHidden = true
    }
    
    /*
        Sets up the common features of a single label node
     */
    private func setupLabelNode(text: String, position: CGPoint) -> SKLabelNode {
        let labelNode = SKLabelNode(fontNamed: "Starjedi")
        labelNode.fontSize = 30
        labelNode.text = text
        labelNode.position = position
        labelNode.zPosition = 1
        return labelNode
    }
    
    /*
        Adds all the HUD elements as children of the GameScene
     */
    public func addObjectsToScene(scene: SKScene) {
        scene.addChild(pauseButton)
        scene.addChild(scoreLabel)
        scene.addChild(pausedLabel)
        scene.addChild(resumeButton)
        scene.addChild(exitButton)
    }
    
    /*
        Used to check whether the game is currently paused or not
     */
    public func isPaused() -> Bool {
        return paused
    }
    
    /*
        Check if the user input has touched any of the pause system buttons
     */
    public func checkIfButtonTouched(location: CGPoint, currentScene: GameScene) -> Bool {
        if self.pauseButton.frame.contains(location) {
            self.pauseButtonPressed()
            return true
        }
        
        if paused {
            if self.resumeButton.frame.contains(location) {
                pauseButtonPressed()
                return true
            }
            
            if self.exitButton.frame.contains(location) {
                let userTotalFunds = UserDefaults.standard.integer(forKey: "money") + score
                UserDefaults.standard.set(userTotalFunds, forKey: "money")
                
                let revealTransition = SKTransition.reveal(with: .right, duration: 1)
                currentScene.view?.presentScene(MenuScene(screenSize: screenSize), transition: revealTransition)
                return true
            }
        }
        
        return false
    }
    
    /*
        Increment the player score by either a passed in amount or just the standard increase
     */
    public func increaseScore(scoreIncrease: Int = Constants.SCORE_INCREMENT) {
        self.score += scoreIncrease
        self.scoreLabel.text = "Score: " + String(self.score)
        self.scoreLabel.position.x = screenSize.width - scoreLabel.frame.width / 2 - 20
    }
    
    /*
        Gets the value of the play score
     */
    public func getScore() -> Int {
        return score
    }
    
    /*
        Called when the user toggles pressing the pause button
     */
    private func pauseButtonPressed() {
        paused = !paused
        
        if paused {
            self.pausedLabel.isHidden = false
            self.resumeButton.isHidden = false
            self.exitButton.isHidden = false
        } else {
            self.pausedLabel.isHidden = true
            self.resumeButton.isHidden = true
            self.exitButton.isHidden = true
        }
    }
    
}
