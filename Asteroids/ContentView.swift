//
//  ContentView.swift
//  Asteroids
//
//  Created by Michael Shepherd on 15/02/2021.
//

import SwiftUI
import SpriteKit

struct ContentView: View {
    @Environment(\.presentationMode) var presentationMode
    
    private var menuScene = MenuScene(screenSize: UIScreen.main.bounds.size)
    
    var body: some View {
        NavigationView {
            SpriteView(scene: menuScene)
                .ignoresSafeArea()
                .navigationBarHidden(true)
        }
        .navigationBarHidden(true)
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear() {
            setupRocketsJSON()
        }
    }
    
    /*
        Checks if the Rockets JSON file can be found in the documents and if not, will create it
     */
    private func setupRocketsJSON() {
        let url = getDocumentsDirectory().appendingPathComponent(Constants.ROCKET_JSON_FILE)
        
        if !FileManager.default.fileExists(atPath: url.path) {
            createRocketsArrayAndStore()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
