//
//  Player.swift
//  Asteroids
//
//  Created by Michael Shepherd on 15/02/2021.
//
//  Handles all the code for the player object that the user controls
//

import Foundation
import SpriteKit

class Player: Object {
    private var bullets: [Bullet] = []
    private var lives: Int
    private var ammo: Int
    private let livesImage: SKSpriteNode
    private var livesLabel: SKLabelNode
    private let ammoImage: SKSpriteNode
    private var ammoLabel: SKLabelNode
    private let rocket: Rocket
    private let screenSize: CGSize
    
    init(position: CGPoint, size: CGSize = Constants.PLAYER_SIZE, rocket: Rocket, screenSize: CGSize) {
        self.rocket = rocket
        self.lives = rocket.lives
        self.ammo = rocket.ammoCapacity
        self.livesLabel = SKLabelNode(fontNamed: "Starjedi")
        self.ammoLabel = SKLabelNode(fontNamed: "Starjedi")
        self.livesImage = SKSpriteNode(imageNamed: "Lives")
        self.ammoImage = SKSpriteNode(imageNamed: "Ammo")
        self.screenSize = screenSize
        super.init(position: position, color: .white, size: size)
        
        setupRocketImageComponents(rocketNode: self, baseColor: getColorFromHexString(colorString: rocket.bodyColorString),
                                                            standColor: getColorFromHexString(colorString: rocket.standColorString),
                                                            windowColor: getColorFromHexString(colorString: rocket.windowColorString))
        
        self.createBullets()
        self.name = "Player"
        self.physicsBody!.categoryBitMask = CollisionMask.Friendly.rawValue
        self.physicsBody!.contactTestBitMask = CollisionMask.Enemy.rawValue
        self.physicsBody!.collisionBitMask = CollisionMask.Nothing.rawValue
        
        self.livesLabel.fontSize = 30
        self.livesLabel.text = ": " + String(lives)
        
        self.ammoLabel.fontSize = 30
        self.ammoLabel.text = ": " + String(ammo)
        
        self.livesImage.size = CGSize(width: 32, height: 32)
        self.ammoImage.size = CGSize(width: 32, height: 32)
        
        self.livesLabel.position = CGPoint(x: screenSize.width - livesLabel.frame.width / 2 - 20, y: screenSize.height - 140)
        self.ammoLabel.position = CGPoint(x: screenSize.width - ammoLabel.frame.width / 2 - 20, y: screenSize.height - 190)
        self.livesImage.position = CGPoint(x: screenSize.width - livesImage.frame.width / 2 - self.livesLabel.frame.width - 25, y: screenSize.height - 130)
        self.ammoImage.position = CGPoint(x: screenSize.width - ammoImage.frame.width / 2 - self.ammoLabel.frame.width - 25, y: screenSize.height - 180)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Updates the Player every tick
     */
    public override func update(screenSize: CGSize) {
        for bullet in bullets {
            bullet.update(screenSize: screenSize)
            
            if bullet.position != position && !bullet.getFired() {
                bullet.position = position
            }
        }
    }
    
    /*
        Moves the player to a passed in x position
     */
    public func move(xPosition: CGFloat, screenWidth: CGFloat) {
        self.position.x = xPosition
        if position.x < 0 + size.width / 2 {
            position.x = size.width / 2
        }
        else if position.x > screenWidth - size.width / 2 {
            position.x = screenWidth - size.width / 2
        }
    }
    
    /*
        Adds all the children of this class to the main GameScene
     */
    public func addChildrenToScene(scene: SKScene) {
        for bullet in bullets {
            scene.addChild(bullet)
        }
        
        scene.addChild(self.livesLabel)
        scene.addChild(self.ammoLabel)
        scene.addChild(self.livesImage)
        scene.addChild(self.ammoImage)
    }
    
    /*
        Fires the next available bullet that the player holds
     */
    public func fireBullet() {
        if ammo >= 1 {
            for bullet in bullets {
                if !bullet.getFired() {
                    bullet.setFired(fired: true)
                    reduceAmmo()
                    return
                }
            }
        }
    }
    
    /*
        Creates the array of bullets that can be fired by the player
     */
    private func createBullets() {
        for _ in 1...Constants.NUM_BULLETS {
            bullets.append(Bullet(position: self.position, color: .green))
        }
    }
    
    /*
        Subtract 1 from the players lives and update the lives label accordingly
     */
    public func changeLives(increase: Bool) {
        if !increase {
            self.lives -= 1
            self.takeDamage()
        } else {
            self.lives += 1
        }
        self.livesLabel.text = ": " + String(lives)
        self.livesLabel.position = CGPoint(x: screenSize.width - livesLabel.frame.width / 2 - 20, y: screenSize.height - 140)
        self.livesImage.position = CGPoint(x: screenSize.width - livesImage.frame.width / 2 - self.livesLabel.frame.width - 25, y: screenSize.height - 130)
    }
    
    /*
        Decreases the players ammo by 1 and updates the ammo label to reflect this
     */
    private func reduceAmmo() {
        self.ammo -= 1
        self.ammoLabel.text = ": " + String(ammo)
        self.ammoLabel.position = CGPoint(x: screenSize.width - ammoLabel.frame.width / 2 - 20, y: screenSize.height - 190)
        self.ammoImage.position = CGPoint(x: screenSize.width - ammoImage.frame.width / 2 - self.ammoLabel.frame.width - 25, y: screenSize.height - 180)
    }
    
    /*
        Gets the current number of lives that the player has
     */
    public func getLives() -> Int {
        return lives
    }
    
    /*
        Increases the current ammo by the passed in amount
     */
    public func increaseAmmo(ammo: Int) {
        self.ammo += ammo
        self.ammoLabel.text = ": " + String(self.ammo)
        self.ammoLabel.position = CGPoint(x: screenSize.width - ammoLabel.frame.width / 2 - 20, y: screenSize.height - 190)
        self.ammoImage.position = CGPoint(x: screenSize.width - ammoImage.frame.width / 2 - self.ammoLabel.frame.width - 25, y: screenSize.height - 180)
    }
}
