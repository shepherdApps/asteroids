//
//  Constants.swift
//  Asteroids
//
//  Created by Michael Shepherd on 15/02/2021.
//

import Foundation
import SpriteKit

enum Constants {
    static let FPS = 60
    static let ROCKET_JSON_FILE: String = "Rockets.json"
    static let PLAYER_SIZE: CGSize = CGSize(width: 40, height: 64)
    static let BULLET_SIZE: CGSize = CGSize(width: 5, height: 10)
    static let ASTEROID_SIZE: CGSize = CGSize(width: 64, height: 64)
    static let COLLECTABLE_SIZE: CGSize = CGSize(width: 32, height: 32)
    static let BULLET_SPEED: CGFloat = 7.0
    static let NUM_BULLETS: Int = 5
    static let STARTING_NUM_ASTEROIDS: Int = 3
    static let MAX_NUM_ASTEROIDS: Int = 7
    static let BASE_ASTEROID_SPEED: CGPoint = CGPoint(x: 0.5, y: 5.0)
    static let MAX_ASTEROID_Y_SPEED: CGFloat = 15.0
    static let ASTEROID_SPEED_INCREASE: CGFloat = 1.0
    static let COLLECTABLE_SPEED: CGPoint = CGPoint(x: 0.2, y: 3.0)
    static let SCORE_INCREMENT: Int = 20
    static let ENEMY_SIZE: CGSize = CGSize(width: 60, height: 38)
    static let BASE_ENEMY_SPEED: CGFloat = 2
    static let BASE_ENEMY_NUM_BULLETS = 2
    static let ENEMY_FIRE_INTERVAL = 2 * FPS
    static let BASE_ENEMY_HEALTH = 5
    static let ARROW_SIZE: CGSize = CGSize(width: 30, height: 40)
    static let AMMO_INCREASE: Int = 5
}
