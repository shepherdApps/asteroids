//
//  HealthBar.swift
//  Asteroids
//
//  Created by Michael Shepherd on 24/02/2021.
//
//  Displays the current health for a given object
//

import Foundation
import SpriteKit

class HealthBar: Object {
    
    private var innerBar: SKSpriteNode = SKSpriteNode()
    private let maxHealth: Int
    private var currentHealth: Int
    private var dead: Bool
    private var decreaseIncrement: CGFloat
    
    /*
        Initializes the health bar
     */
    init(position: CGPoint, size: CGSize, startingHealth: Int) {
        self.maxHealth = startingHealth
        self.currentHealth = startingHealth
        self.dead = false
        self.decreaseIncrement = 0
        super.init(position: position, color: .lightGray, size: size)
        
        innerBar = SKSpriteNode(color: .green, size: CGSize(width: size.width - 6, height: size.height - 4))
        innerBar.position = CGPoint(x: 0, y: 0)
        decreaseIncrement = innerBar.size.width / CGFloat(maxHealth)
        
        self.physicsBody!.contactTestBitMask = CollisionMask.Nothing.rawValue
        self.physicsBody!.collisionBitMask = CollisionMask.Nothing.rawValue
        
        self.addChild(innerBar)
    }
    
    /*
        Required method
     */
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
        Each frame, move the bar so that it has the same x position as its parent
     */
    public func update() {
        self.position.x = (self.parent as! Object).position.x
    }
    
    /*
        Subtract 1 from the health and update the bar to show this change
    */
    public func decreaseHealth() {
        currentHealth -= 1
        
        self.innerBar.size.width -= decreaseIncrement
        
        if currentHealth == maxHealth / 4  {
            innerBar.color = .red
        }
        else if currentHealth == maxHealth / 2 {
            innerBar.color = .yellow
        }
        else if currentHealth == 0 {
            dead = true
        }
    }
    
    /*
        Gets whether the parent of this health bar should be dead
     */
    public func getDead() -> Bool {
        return dead
    }
}
